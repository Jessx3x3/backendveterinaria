const express = require('express');
const morgan = require('morgan');
const formidable = require('express-form-data');

const app = express();

const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/veterinaria', {
    useNewUrlParser: true 
})
    .then(db => console.log('DB is connected'))
    .catch(err => console.log(err));

//Configuración
app.set('port', process.env.PORT || 3000);

//Middlewares
app.use(morgan('dev'));
app.use(express.json());
app.use(formidable.parse({ keepExtensions: true}));

//Routes
app.use('/comentarios', require('./routes/comentarios'));
app.use('/articulos', require('./routes/articulos'));

//Filas estáticas
app.use(express.static(__dirname + '/public'));

//Escuchando
app.listen(app.get('port'), () => {
    console.log(`server on port ${app.get('port')}`);
  });