const mongoose = require('mongoose');
const { Schema } = mongoose;

const Comentario = new Schema({
    email: String,
    comentario: String
});
module.exports = mongoose.model('Comentario', Comentario);