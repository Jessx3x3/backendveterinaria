const mongoose = require('mongoose');
const { Schema } = mongoose;

const Articulo = new Schema({
    titulo: String,
    subtitulo: String,
    imgUrl: String,
    contenido: String,
});
module.exports = mongoose.model('Articulo', Articulo);