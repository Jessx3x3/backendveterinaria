const express = require('express');
const router = express.Router();

const Comentario = require('../models/Comentario');

//Obtener Comentarios
router.get('/', async (req, res) => {
    const comentarios = await Comentario.find();
    res.json(comentarios);
});

//Crear Comentarios
router.post('/', async (req, res) => {
    const coment = new Comentario(req.body);
    await coment.save();
    res.json({
        status: 'Comentario Creado'
      });
});

//Obtener Comentario específico
router.get('/:id', async (req, res) => {
    const coment = await Comentario.findById(req.params.id);
    res.json(coment);
  });

//Actualizar Comentarios
router.put('/:id', async (req, res) => {
    await Comentario.findByIdAndUpdate(req.params.id, req.body);
    res.json({
        status: 'Comentario Actualizado'
      });
});

//Eliminar Comentarios
router.delete('/:id', async (req, res) => {
    await Comentario.findByIdAndRemove(req.params.id);
    res.json({
      status: 'Comentario Eliminado'
    });
  });

module.exports = router;