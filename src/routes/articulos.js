const express = require('express');
const router = express.Router();

const Articulo = require('../models/Articulo');

//Obtener Articulo
router.get('/', async (req, res) => {
    const articulos = await Articulo.find();
    res.json(articulos);
});

//Crear Articulo
router.post('/', async (req, res) => {
    const articulos = new Articulo(req.body);
    await articulos.save();
    res.json({
        status: 'Articulo Creado'
      });
});

//Obtener Articulo específico
router.get('/:id', async (req, res) => {
    const articulos = await Articulo.findById(req.params.id);
    res.json(articulos);
  });

//Actualizar Articulo
router.put('/:id', async (req, res) => {
    await Articulo.findByIdAndUpdate(req.params.id, req.body);
    res.json({
        status: 'Articulo Actualizado'
      });
});

//Eliminar Articulo
router.delete('/:id', async (req, res) => {
    await Articulo.findByIdAndRemove(req.params.id);
    res.json({
      status: 'Articulo Eliminado'
    });
  });

module.exports = router;